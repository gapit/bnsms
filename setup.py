
from setuptools import setup, find_packages

version = '0.0.1'

setup(
    name="bnsms",
    version=version,
    description='Alerta plugin for BN SMS',
    license='MIT',
    author='Gapit',
    packages=find_packages(),
    py_modules=['bnsms'],
    install_requires=[
        'requests'
    ],
    include_package_data=True,
    zip_safe=True,
    entry_points={
        'alerta.plugins': [
            'bnsms = bnsms:SendSMSMessage'
        ]
    }
)
