import logging
import os
import requests
import json

try:
    from alerta.plugins import app  # alerta >= 5.0
except ImportError:
    from alerta.app import app  # alerta < 5.0
from alerta.plugins import PluginBase

LOG = logging.getLogger('alerta.plugins.bnsms')

BN_SMS_URL = os.environ.get('BN_SMS_URL') or app.config['BN_SMS_URL']

class SendSMSMessage(PluginBase):

    def pre_receive(self, alert):
        return alert

    def post_receive(self, alert):
        if alert.repeat:
            return
        body = alert.get_body(history=False)


        url = BN_SMS_URL
        requests.post(url, json={'createtime': body['createTime'], 'customer': body['customer'], 'environment': body['environment'], 'duplicatecount': body['duplicateCount'], 'event': body['event'], 'group': body['group'], 'lastreceivetime': body['lastReceiveTime'], 'origin': body['origin'], 'resource': body['resource'], 'service': body['service'], 'severity': body['severity'], 'tags': body['tags'], 'text': body['text'], 'value': body['value'], 'type': body['type']})

        LOG.info("BN SMS Message ID: %s", alert)

    def status_change(self, alert, status, text):
        return
