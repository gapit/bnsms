BN SMS Plugin
=================

For help, join [![Gitter chat](https://badges.gitter.im/alerta/chat.png)](https://gitter.im/alerta/chat)

Installation
------------

Clone the GitHub repo and run:

    $ python setup.py install

Or, to install remotely from GitHub run:

    $ pip install git+https://bitbucket.org/gapit/bn_sms.git

Note: If Alerta is installed in a python virtual environment then plugins
need to be installed into the same environment for Alerta to dynamically
discover them.

Configuration
-------------

Add `bn_sms` to the list of enabled `PLUGINS` in `alertad.conf` server
configuration file and set plugin-specific variables either in the
server configuration file or as environment variables.

```python
PLUGINS = ['bn_sms']
BN_SMS_IP = '10.0.0.109'
BN_SMS_PORT = '8999'
```

Troubleshooting
---------------

Restart Alerta API and confirm that the plugin has been loaded and enabled.


License
-------

Copyright (c) 2018 Gapit. Available under the MIT License.
