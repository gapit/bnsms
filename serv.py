from flask import Flask, request

app = Flask(__name__)

@app.route('/', methods=['GET'])
def hello():
    return 'Hello, World!'

@app.route('/sms', methods=['POST'])
def parse_request():
    data = request.get_json()
    print(data)
    return "OK", 200

if __name__ == '__main__':
      app.run(host='0.0.0.0', port=8999)
